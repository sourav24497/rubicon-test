## RubiCon Rest Apis Docs

#### Technology Stack
- Java Spring Boot
- JPA
- Cron Scheduling
- H2 Database

#### Run the application and server will be started on port 9001.

#### Base URL http://localhost:9001

#### Api Information

###### Create Order (POST - /orders/)
- Header Information
```
Content-Type : application/json
```

- Request Format
```json
{
    "farmId":1,
    "duration":3,
    "startDateTime":"25-06-2021 05:08:00",
    "status":"Created"
}
```
- Response Format
```json
{
    "error": "False",
    "success": "Order Created Successfully",
    "object": {
        "orderId": 2,
        "farmId": 2,
        "duration": 3,
        "startDateTime": "25-06-2021 05:08:00",
        "status": "Created"
    }
}
```
- Error Response Format
```json
{
    "error": "Order already present for the same date",
    "success": "False",
    "object": {
        "orderId": null,
        "farmId": 1,
        "duration": 3,
        "startDateTime": "25-06-2021 05:08:00",
        "status": "Not Created"
    }
}
```

###### Get Order (GET - /orders/:orderId)
- Reponse Format
```json
{
    "error": "False",
    "success": "Order Fetched Successfully",
    "object": {
        "orderId": 1,
        "farmId": 1,
        "duration": 3,
        "startDateTime": "25-06-2021 05:08:00",
        "status": "Delivered"
    }
}
```
- Error Response Format
```json
{
    "error": "No Such Id",
    "success": "False",
    "object": null
}
```

###### Cancel Order (PATCH - /orders/:orderId)
- Response Format
```json
{
    "error": "False",
    "success": "Order Cancelled Successfully",
    "object": {
        "orderId": 2,
        "farmId": 1,
        "duration": 3,
        "startDateTime": "25-06-2021 10:08:00",
        "status": "Cancelled"
    }
}
```
- Error Response Format
```json
{
    "error": "No Such Id",
    "success": "False",
    "object": null
}
```

