package Rubicon.Wrapper;

import Rubicon.entity.WaterOrder;

public class Wrapper {
	
	private String error;
	private String success;
	private WaterOrder object;
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public WaterOrder getObject() {
		return object;
	}
	public void setObject(WaterOrder object) {
		this.object = object;
	}
	
	

}
