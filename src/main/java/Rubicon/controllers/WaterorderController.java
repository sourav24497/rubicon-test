package Rubicon.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Rubicon.Wrapper.Wrapper;
import Rubicon.entity.WaterOrder;
import Rubicon.service.WaterOrderService;


@RestController
@RequestMapping("/orders")
public class WaterorderController {
	@Autowired
	private WaterOrderService waterOrderService;
	
	@PostMapping("/")
	public Wrapper createOrder(@RequestBody WaterOrder waterOrder) throws Exception
	{
		//Logger to be implemented.
		return waterOrderService.createOrder(waterOrder);
	}
	@GetMapping("/{id}")
	public Wrapper getOrder(@PathVariable("id") Long orderId)
	{
		return waterOrderService.getWaterOrderById(orderId);
	}
	@PatchMapping("/{id}")
	public Wrapper cancelOrder(@PathVariable("id") Long orderId)
	{
		return waterOrderService.cancelWaterOrder(orderId);
	}
	
	
}
