package Rubicon.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class WaterOrder {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long orderId;
	private Long farmId;
	private int duration;
	private String startDateTime;
	private String status;
	@Override
	public String toString() {
		return "WaterOrder [orderId=" + orderId + ", farmId=" + farmId + ", duration=" + duration + ", startDateTime="
				+ startDateTime + ", status=" + status + "]";
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Long getFarmId() {
		return farmId;
	}
	public void setFarmId(Long farmId) {
		this.farmId = farmId;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}
	

	
}
