package Rubicon.repository;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import Rubicon.entity.WaterOrder;

@Repository
public interface WaterOrderRepository extends JpaRepository<WaterOrder, Long> {

	
@Query(value="select w from WaterOrder w where w.farmId = ?1")
public List<WaterOrder> isOverlap(Long startdatetime);
	
@Modifying
@Transactional
@Query(value="update WaterOrder w set w.status = ?2 where w.id = ?1")
public void updateOrder(Long orderId,String cancel);

@Query(value="select w from WaterOrder w where w.status = ?1")
public List<WaterOrder> getOrdersByStatus(String status);

}
