package Rubicon.service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import Rubicon.Wrapper.Wrapper;
import Rubicon.entity.WaterOrder;
import Rubicon.repository.WaterOrderRepository;

@Service
public class WaterOrderService {

	@Autowired
	private WaterOrderRepository waterOrderRepository;
	private static final Logger log = LoggerFactory.getLogger(WaterOrderService.class);
	
	
	private Date getDate(String startDateTime) throws Exception
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy kk:mm");
		return formatter.parse(startDateTime);
	}
	private Date getDate(Date farmStartDate, int duration)
	{
		Calendar cl = Calendar.getInstance();
		cl.setTime(farmStartDate);
		cl.add(Calendar.HOUR,duration);
		return cl.getTime();
	}
	
	public Wrapper createOrder(WaterOrder waterOrder) throws ParseException, Exception
	{
		Wrapper wrapper = new Wrapper();
		List<WaterOrder> w = waterOrderRepository.isOverlap(waterOrder.getFarmId());
		if(!w.isEmpty())
		{
			Iterator<WaterOrder> it = w.iterator();
			while(it.hasNext())
			{
				WaterOrder wOrder = it.next();
				Date farmStartDate = getDate(wOrder.getStartDateTime());
				Date orderStartDate = getDate(waterOrder.getStartDateTime());
				Date farmEndDate = getDate(farmStartDate,wOrder.getDuration());
				if(orderStartDate.compareTo(farmStartDate)>=0&&orderStartDate.compareTo(farmEndDate)<=0)
				{
					try 
					{
						throw new Exception ("Order already present for the same date");
					}
					catch (Exception e)
					{
						waterOrder.setStatus("Not Created");
						wrapper.setError(e.getMessage());
						wrapper.setSuccess("False");
						wrapper.setObject(waterOrder);
						return wrapper;
					}
				}
				
			}
		}
		
		wrapper.setError("False");
		wrapper.setSuccess("Order Created Successfully");
		wrapper.setObject(waterOrderRepository.save(waterOrder));
		return wrapper;
				
	}
	public Wrapper getWaterOrderById(Long orderId)
	{
		Wrapper wrapper = new Wrapper();
		try
		{
			Optional<WaterOrder> wo = waterOrderRepository.findById(orderId);
			WaterOrder wOrder = wo.get();
			wrapper.setError("False");
			wrapper.setSuccess("Order Fetched Successfully");
			wrapper.setObject(wOrder);
			System.out.println(wo.toString());
		}
		catch (Exception e)
		{
			wrapper.setError("No Such Id");
			wrapper.setSuccess("False");
			wrapper.setObject(null);
		}
		return wrapper;
	}
	public Wrapper cancelWaterOrder(Long orderId)
	{
		Wrapper wrapper = new Wrapper();
		try
		{
			WaterOrder wOrder = waterOrderRepository.findById(orderId).get();
			if(wOrder.getStatus().equalsIgnoreCase("delivered"))
			{
				wrapper.setError("Order is already delivered");
				wrapper.setSuccess("False");
				wrapper.setObject(wOrder);
			}
			else
			{
				waterOrderRepository.updateOrder(orderId,"Cancelled");
				wrapper.setError("False");
				wrapper.setSuccess("Order Cancelled Successfully");
				wrapper.setObject(waterOrderRepository.findById(orderId).get());
				
			}
		}
		catch (Exception e)
		{
			wrapper.setError("No Such Id");
			wrapper.setSuccess("False");
			wrapper.setObject(null);
		}
		
		
		return wrapper;
	}

	
	@Scheduled(cron = "1 * * * * *")
	public void scheduleFixedDelayTask() throws Exception {
		long millis=System.currentTimeMillis();  
		Date currDate=new Date(millis);
		List<WaterOrder> getOrdersByStatus = waterOrderRepository.getOrdersByStatus("Created");
		Iterator<WaterOrder> it = getOrdersByStatus.iterator();
		while(it.hasNext())
		{
			WaterOrder wOrder = it.next();
			Date farmStartDate = getDate(wOrder.getStartDateTime());
			if(currDate.compareTo(farmStartDate)>=0)
			{
				waterOrderRepository.updateOrder(wOrder.getOrderId(),"InProgress");
				log.info("Water order for farm id "+wOrder.getFarmId()+" is Started");
				
			}
			
		}
		getOrdersByStatus = waterOrderRepository.getOrdersByStatus("InProgress");
		it = getOrdersByStatus.iterator();
		while(it.hasNext())
		{
			WaterOrder wOrder = it.next();
			Date farmStartDate = getDate(wOrder.getStartDateTime());
			Date farmEndDate = getDate(farmStartDate,wOrder.getDuration());
			if(currDate.compareTo(farmEndDate)>=0)
			{
				
				waterOrderRepository.updateOrder(wOrder.getOrderId(),"Delivered");
				log.info("Water order for farm id "+wOrder.getFarmId()+" is Delivered");
			}
			
		}	
	}
	
}
